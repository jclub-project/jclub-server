package database

import "github.com/jinzhu/gorm"

// Novel represent a novel saved in the database
type Novel struct {
	gorm.Model
	Name string
	Slug string
}

// InsertNovel Insert a novel inside database
func (c *Client) InsertNovel(name, slug string) error {
	return c.Gorm.Create(Novel{
		Name: name,
		Slug: slug,
	}).Error
}

// UpdateNovel Insert a novel inside database
func (c *Client) UpdateNovel(id uint, name, slug string) error {
	return c.Gorm.Model(&Novel{}).Where("id = ?", id).Update(Novel{Name: name, Slug: slug}).Error
}

// DeleteNovel Insert a novel inside database
func (c *Client) DeleteNovel(id uint) error {
	return c.Gorm.Where("id = ?", id).Delete(&Novel{}).Error
}
