// Package database represent model & method to insert/update/delete record inside database
package database

import (
	"github.com/jinzhu/gorm"
	// Used by gorm
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

var database *Client = nil

type Client struct {
	Gorm *gorm.DB
}

// ConnectToDB Create a singleton to db
func connectToDB() (*Client, error) {
	db, err := gorm.Open("sqlite3", "data.db")
	if err != nil {
		return nil, err
	}

	db.AutoMigrate(&Novel{})

	database = &Client{
		Gorm: db,
	}

	return database, nil
}

// GetDB Get database singleton and connect to db before getting it if not set
func GetDB() (*Client, error) {
	if database == nil {
		db, err := connectToDB()
		if err != nil {
			return nil, err
		}

		return db, nil
	}

	return database, nil
}
