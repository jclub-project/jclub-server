package main

import (
	"jclub_server/database"
	"log"

	"github.com/pkg/errors"
)

func main() {
	log.Println("Starting JClub Server")
	client, err := database.GetDB()
	if err != nil {
		panic(errors.Wrap(err, "Can't connect to database"))
	}
	defer client.Gorm.Close()
}
